﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComoLambdas
{
    public class CompanyToken
    {
        public int CompanyId { get; set; }
        public string AuthToken { get; set; }
    }
}
