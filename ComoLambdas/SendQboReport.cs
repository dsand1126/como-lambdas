using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Amazon.Lambda.Core;
using Amazon.S3;
using Amazon.S3.Model;

using RestClient.Api;
using System.Xml.Serialization;

using TelemetryManager;

using System.IO;
using System.Net;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
//[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace ComoLambdas
{
    public class SendQboReport
    {
        private static readonly string ApplicationName = "ComoLambdas";
        private static readonly string AppEnvironment = "dev";
        private static readonly string LogConfigLocation = "log.config";
        private static readonly ILogger _Log = new Log4NetLogger("Como Lambdas", ApplicationName, AppEnvironment, LogConfigLocation);

        /// <summary>
        /// Once a new file is dumped into the S3 bucket, this lambda will fire
        /// The lambda will then make an API Post call to the Company API with the retrieved data
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string SendQboReportFunction(object input, ILambdaContext context)
        {
            var apiUrl = "https://companyapi.lighter.capital/api/v1/Company/Profile";

            var client = new ApiCall();
            var response = client.Post(apiUrl, input);

            return $"response: {response}";
        }
    }
}
