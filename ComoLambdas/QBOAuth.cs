﻿using Intuit.Ipp.Core;
using Intuit.Ipp.ReportService;
using Intuit.Ipp.Security;
using Intuit.Ipp.Data;

using TelemetryManager;

namespace ComoLambdas
{
    public static class QBOAuth
    {
        //hardcoded for testing 
        private static readonly string ApplicationName = "ComoLambdasLAMBDA";
        private static readonly string AppEnvironment = "dev";

        private static readonly string LogConfigLocation = "log.config";
        private static readonly ILogger _Log = new Log4NetLogger("Como Lambdas", ApplicationName, AppEnvironment, LogConfigLocation);

        public static Report GetReport(string reportName, Customer customer)
        {
            _Log.Info($"GetReport function called, calling GetService");

            ReportService service = GetService(customer);

            _Log.Info($"GetService returned new service, execute report");

            //lambda breaks here, something in ExecuteReport code causes it to fail
            //sending back an empty report will succeed (comment out report2 first to see it works)
            var report2 = service.ExecuteReport(reportName);
            var report = new Report();

            _Log.Info($"Report: {report2}");

            return report;
        }

        private static ReportService GetService(Customer customer)
        {
            _Log.Info($"GetService function called, creating new ServiceContext");

            ServiceContext ctx = new ServiceContext(customer.QuickBooksRealmId, IntuitServicesType.QBO, new OAuth2RequestValidator(customer.QuickBooksAccessToken));

            ctx.IppConfiguration.BaseUrl.Qbo = "https://sandbox-quickbooks.api.intuit.com/";

            _Log.Info($"Service context created, returning new ReportService");

            return new ReportService(ctx);
        }

    }
}
