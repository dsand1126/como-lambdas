using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Amazon.Lambda.Core;
using Amazon.S3;
using Amazon.S3.Model;

using RestClient.Api;
using System.Xml.Serialization;

using TelemetryManager;

using System.IO;
using System.Net;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace ComoLambdas
{
    public class Function
    {
        private static readonly string ApplicationName = "ComoLambdas";
        private static readonly string AppEnvironment = "dev";
        private static readonly string LogConfigLocation = "log.config";
        private static readonly ILogger _Log = new Log4NetLogger("Como Lambdas", ApplicationName, AppEnvironment, LogConfigLocation);

        private static readonly string _BASE_URL = "sandbox-quickbooks.api.intuit.com";

        /// <summary>
        /// An API Gateway POST request makes a call to this lambda
        /// This lambda makes an API Get Request that dumps the returned JSON object into an S3 bucket
        /// A CompanyToken object is needed to make authorize the API request
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<string> ApiGetRequestToS3(Customer customer, ILambdaContext context)
        {
            try
            {
                //_Log.Info($"Starting {ApplicationName} App");
                return await QboHttpRequest(customer);
            }
            catch (Exception ex)
            {
                //_Log.Error($"Error generating report: {ex}");
                return $"error: {ex}";
            }
        }

        private async Task<string> QboHttpRequest(Customer customer)
        {
            string uri = string.Format("https://{0}/v3/company/{1}/reports/{2}?minorversion=47",
                _BASE_URL,
                customer.QuickBooksRealmId,
                "ProfitAndLoss");

            // send the request
            HttpWebRequest qboApiRequest = (HttpWebRequest)WebRequest.Create(uri);
            qboApiRequest.Method = "GET";
            qboApiRequest.Headers.Add(string.Format("Authorization: Bearer {0}", customer.QuickBooksAccessToken));
            qboApiRequest.ContentType = "application/json;charset=UTF-8";
            qboApiRequest.Accept = "*/*";


            // get the response
            HttpWebResponse qboApiResponse = (HttpWebResponse)qboApiRequest.GetResponse();
            if (qboApiResponse.StatusCode == HttpStatusCode.Unauthorized)//401
            {
                Console.WriteLine("Invalid/Expired Access Token.");
                return "invalid token";
            }
            else
            {
                //read qbo api response
                using (var qboApiReader = new StreamReader(qboApiResponse.GetResponseStream()))
                {
                    string responseText = qboApiReader.ReadToEnd();
                    string reportXml;

                    using (var stringwriter = new StringWriter())
                    {
                        var serializer = new XmlSerializer(responseText.GetType());
                        serializer.Serialize(stringwriter, responseText);

                        reportXml = stringwriter.ToString();
                    }

                    await PutS3Object("dana-test-bucket", "ProfitAndLossReport-CustomerID=58", reportXml);

                    return reportXml;
                }
            }
        }

        public static async Task<bool> PutS3Object(string bucket, string key, string content)
        {
            try
            {
                var accessKey = "AKIAT7LXBPN2CQYJR3FY";
                var secretKey = "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";
                using (var client = new AmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.USWest2))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = bucket, //s3 bucket name
                        Key = key, //name of file
                        ContentBody = content //file contents
                    };
                    var response = await client.PutObjectAsync(request);
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in PutS3Object:" + ex.Message);
                return false;
            }
        }

        ///// <summary>
        ///// An API Gateway POST request makes a call to this lambda
        ///// This lambda makes an API Get Request that dumps the returned JSON object into an S3 bucket
        ///// A CompanyToken object is needed to make authorize the API request
        ///// </summary>
        ///// <param name="input"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public async Task<string> ApiGetRequestToS3BROKEN(Customer customer, ILambdaContext context)
        //{
        //    _Log.Info($"Starting {ApplicationName} App");

        //    try
        //    {
        //        var report = QBOAuth.GetReport("ProfitAndLoss", customer);
        //        //var report = QBOAuth.GetReport("BalanceSheet", customer);
        //        string reportXml;

        //        _Log.Info($"Report retrieved from QBO, serializing to XML");

        //        //var reportJson = JsonConvert.SerializeObject(report);

        //        using (var stringwriter = new System.IO.StringWriter())
        //        {
        //            var serializer = new XmlSerializer(report.GetType());
        //            serializer.Serialize(stringwriter, report);

        //            reportXml = stringwriter.ToString();
        //            Console.WriteLine(reportXml);
        //            _Log.Info($"XML report: {reportXml}");
        //        }

        //        _Log.Info($"Sending report to S3 bucket");

        //        await PutS3Object("dana-test-bucket", "ProfitAndLossReport-CustomerID=58", reportXml);

        //        _Log.Info($"Report saved in bucket, lambda complete.");

        //        return $"response: {reportXml}";
        //    }
        //    catch (Exception ex)
        //    {
        //        _Log.Error($"Error generating report: {ex}");
        //        return $"error: {ex}";
        //    }
        //}
    }
}
