﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ComoLambdas
{
    public class NasaObject
    {
        public string Date { get; set; }
        public string Explanation { get; set; }
        public string Hdurl { get; set; }
        public string MediaType { get; set; }
        public string ServiceVersion { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
    }
}
