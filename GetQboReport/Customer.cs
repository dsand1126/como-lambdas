﻿using System;

namespace GetQboReport
{
    public class Customer
    { 
        public string QuickBooksAccessToken { get; set; }

        public string QuickBooksRealmId { get; set; }

        public string ReportType { get; set; }
    }
}