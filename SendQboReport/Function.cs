using Amazon.Lambda.Core;

using RestClient.Api;
using System;
using System.Threading.Tasks;
using TelemetryManager;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace SendQboReport
{
    public class Function
    {
        private static readonly string ApplicationName = "Lambda-SendQboReport";
        private static readonly string AppEnvironment = "dev";
        private static readonly string LogConfigLocation = "log.config";
        private static readonly ILogger _Log = new Log4NetLogger("Como Lambdas", ApplicationName, AppEnvironment, LogConfigLocation);

        public static async Task<string> FunctionHandler(string input)
        {
            return await SendQboReport(input);
        }

        public static async Task<string> SendQboReport(string input)
        {
            try
            {
                _Log.Info($"Starting {ApplicationName} App");
                return await CompanyApiCall(input);
            }
            catch (Exception ex)
            {
                _Log.Error($"Error generating report: {ex}");
                return $"error: {ex}";
            }
        }

        public static async Task<string> CompanyApiCall(string input)
        {
            var apiUrl = "https://companyapi.lighter.capital/api/v1/Company/Profile";

            var client = new ApiCall();
            var response = await client.PostAsync(apiUrl, input);

            return $"response: {response}";
        }
    }
}
