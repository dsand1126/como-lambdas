﻿using System;

namespace Workbench
{
    public class Customer
    { 
        public string QuickBooksAccessToken { get; set; }

        public string QuickBooksRefreshToken { get; set; }

        public string QuickBooksRealmId { get; set; }

    }
}