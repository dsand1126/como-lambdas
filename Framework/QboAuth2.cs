﻿using Intuit.Ipp.Core;
using Intuit.Ipp.ReportService;
using Intuit.Ipp.Security;
using Report = Intuit.Ipp.Data.Report;

using TelemetryManager;

namespace Workbench
{
    public static class QBOAuth2
    {
        //hardcoded for testing 
        private static readonly string ApplicationName = "ComoLambdasCONSOLE";
        private static readonly string AppEnvironment = "dev";

        private static readonly string LogConfigLocation = "log.config";
        private static readonly ILogger _Log = new Log4NetLogger("Como Lambdas", ApplicationName, AppEnvironment, LogConfigLocation);

        public static Report GetReport(string reportName, Customer customer)
        {
            _Log.Info($"GetReport function called, calling GetService");

            ReportService service = GetService(customer);

            _Log.Info($"GetService returned new service, execute report");

            var report = service.ExecuteReport(reportName);

            _Log.Info($"Report: {report}");

            return report;
        }

        private static ReportService GetService(Customer customer)
        {
            _Log.Info($"GetService function called, creating new ServiceContext");

            ServiceContext ctx = new ServiceContext(customer.QuickBooksRealmId, IntuitServicesType.QBO, new OAuth2RequestValidator(customer.QuickBooksAccessToken));

            ctx.IppConfiguration.BaseUrl.Qbo = "https://sandbox-quickbooks.api.intuit.com/";

            _Log.Info($"Service context created, returning new ReportService");

            return new ReportService(ctx);
        }
    }
}
