﻿using Newtonsoft.Json;
using RestClient.Api;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Xml.Serialization;

using TelemetryManager;

namespace Workbench
{
    /// <summary>
    /// The workbench app is intended to be used by devs for testing out code as it is written 
    /// </summary>
    public class Program
    {
        //hardcoded for testing 
        private static readonly string ApplicationName = "ComoLambdasCONSOLE";
        private static readonly string AppEnvironment = "dev";

        private static readonly string LogConfigLocation = $"{AppDomain.CurrentDomain.BaseDirectory}/{Log4NetLogger.LOG4NET_CONFIG_FILENAME}";
        private static readonly ILogger _Log = new Log4NetLogger("Como Lambdas", ApplicationName, AppEnvironment, LogConfigLocation);

        private static readonly string _RealmId = "1292692610";
        private static readonly string _AccessToken = "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..iHenlD0OhnG2wAi2rBXIuA.xHBPYBT1dqQmnqxbVuRYtmj4FLfZy_ilT_qBdjInlJUJ4OW6UvNJgUugpHIMJpv2pM19PQg5RfPe9P6y4E1xi9vgRAG2sqkILTJyDEnxETbtOBtySnts5qbq3maRyu5KwrWl-aCn5-YQeDVIZVC6UPBCsDf2B_uClLzVsw67B7ytASRmG5AmupM1TFpy1G05KebbjtnhUhZgIhXhow59ojlyAURj7zddOSdW9TJ_gZLFgaNfdYeb2RO6MpG821h06753Mf-E362_EQXOwZDQzMYWDLtt-EcIG4tf1bM2Qyz_DYTIyjA1gCPV3S-Nk85Jo0AUNT-_49267gaZLnLadML-tXti9bg4dhOJAlpc_tmkI0sxx1TdQWTX1ETphpDTkCfjgn4G6YRrk1W9mz4x0r2DCvEft4g4ZOoz_JAsru9qVZHNMGsiqzIQeaqATmD5mKxMtxX_JJGAFQGhAGeCs9SPXz7RTxe2YZhGB_IYasMAprTPSfX5gD5VeB_I9ZhSYwJBWujD6iQ9IQWhY533W05VeeYKY37dGXWXT6qC9Z8rY5RamYvezADy_1Eb1BQjU3_ryo7LUWruZ-6NLokgl1FZStKWs6e1mAPm0CrM24nAZqAVjvKSWLKWSQyvjWHRGhMCm8EJk993SzivzQHr_1kJRmepbecoztlqR3JoTuzRMhI_iRZtYNGeO9W575njPx9Xa0nJpQ12C1PSEHMjSTBT_B5pYZ5AkMpBgi1NSws.kVhroSIpg0ZRWKCf7lelCg";

        public static void Main(string[] args)
        {
            //ApiGetRequestToS3BROKEN();

            ApiGetRequestToS3();
        }

        public static void ApiGetRequestToS3()
        {
            _Log.Info($"Starting {ApplicationName} App");

            try
            {
                QboApiCall(_AccessToken, _RealmId);
            }
            catch (Exception ex)
            {
                _Log.Error($"Error generating report: {ex}");
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        public static void QboApiCall(string access_token, string realmId)
        {
            try
            {
                if (realmId != "")
                {
                    string uri = string.Format("https://{0}/v3/company/{1}/reports/ProfitAndLoss?minorversion=47", "sandbox-quickbooks.api.intuit.com", realmId);

                    // send the request
                    HttpWebRequest qboApiRequest = (HttpWebRequest)WebRequest.Create(uri);
                    qboApiRequest.Method = "GET";
                    qboApiRequest.Headers.Add(string.Format("Authorization: Bearer {0}", access_token));
                    qboApiRequest.ContentType = "application/json;charset=UTF-8";
                    qboApiRequest.Accept = "*/*";

                    // get the response
                    HttpWebResponse qboApiResponse = (HttpWebResponse)qboApiRequest.GetResponse();
                    if (qboApiResponse.StatusCode == HttpStatusCode.Unauthorized)//401
                    {
                        Console.WriteLine("Invalid/Expired Access Token.");
                    }
                    else
                    {
                        //read qbo api response
                        using (var qboApiReader = new StreamReader(qboApiResponse.GetResponseStream()))
                        {
                            string responseText = qboApiReader.ReadToEnd();
                            Console.WriteLine($"{responseText}");
                        }
                    }
                }

            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {

                        Console.WriteLine("HTTP Status: " + response.StatusCode);
                        var exceptionDetail = response.GetResponseHeader("WWW-Authenticate");
                        if (exceptionDetail != null && exceptionDetail != "")
                        {
                            Console.WriteLine(exceptionDetail);
                        }
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // read response body
                            string responseText = reader.ReadToEnd();
                            if (responseText != null && responseText != "")
                            {
                                Console.WriteLine(responseText);
                            }
                        }
                    }

                }
            }
        }

        public static void ApiGetRequestToS3BROKEN()
        {
            _Log.Info($"Starting {ApplicationName} App");

            try
            {
                var customer = new Customer
                {
                    QuickBooksAccessToken = "LongAssAccessTokenHere",
                    QuickBooksRefreshToken = "AB11593109885UrUUM7QPKM5vcoLN8eFdz4SOGG9gLIZpSon83",
                    QuickBooksRealmId = "1292692610"
                };

                var report = QBOAuth2.GetReport("ProfitAndLoss", customer);
                //var report = QBOAuth2.GetReport("BalanceSheet", customer);

                _Log.Info($"Report retrieved from QBO, serializing to XML");

                //var reportJson = JsonConvert.SerializeObject(report);

                using (var stringwriter = new System.IO.StringWriter())
                {
                    var serializer = new XmlSerializer(report.GetType());
                    serializer.Serialize(stringwriter, report);

                    var reportXml = stringwriter.ToString();
                    Console.WriteLine(reportXml);
                    _Log.Info($"XML report: {reportXml}");
                }
            }
            catch (Exception ex)
            {
                _Log.Error($"Error generating report: {ex}");
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
